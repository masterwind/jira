// ==UserScript==
// @name         jira lite
// @description  Костыли jira
// @namespace    jira-lite-masterwind
// @author       masterwind
// @version      20.8.18
// @updateURL    https://jira-lite.netlify.app/jira.user.js
// @downloadURL  https://jira-lite.netlify.app/jira.user.js
// @supportURL   https://bitbucket.org/masterwind/jira/issues/
// @include      https://jira.uid.me/*
// @require      https://utils-masterwind.netlify.com/utils.js?v=1573510479088
// @icon         https://jira-lite.netlify.app/logo.png
// @grant        none
// @noframes
// ==/UserScript==


;((APP, LOC, WIN, DOC, LS, SS, DATE, D, M, Y, stp ) => {
stp.DEBUG && (
	console.clear(),
	console.log(`APP`, APP),
	console.log(`LOC`, LOC, `WIN`, WIN),
	console.log(`DOC`, DOC),
	console.log(`LS`, LS),
	console.log(`SS`, SS),
	console.log(`DATE`, DATE, `D`, D, `M`, M, `Y`, Y),
	console.log(`stp`, stp)
);


	//// кнопка бранча
stp.DEBUG && console.log(`/browse/UCOZ | /browse/IN`, utils.location.has('/browse/UCOZ') , utils.location.has('/browse/IN') );
	utils.location.any(['/browse/UCOZ', '/browse/IN']) && window.issuedetails && (
		!window.rowForcustomfield_10200
		// create branch button
		&& !window.issuedetails.insertAdjacentHTML('beforeend',
				utils.template.render(tpl.fieldContainer, {
					listId    : 'rowForcustomfield_10200',
					wrapperId : 'createBranchContainer',
					name      : 'Branch',
					button    : utils.template.render(tpl.button.common, {
						id      : 'createBranchButton',
						onclick : 'APP.saveBranch()',
						value   : 'Create new branch',
					}),
				})
		)
		// copy branch button
		|| (
			(window.customBranchField = window['customfield_10200-val']) && (
				// customBranchField.textContent.trim().split(/[,|\s]/g).filter(item => !!item).map(branch => {
				APP.getAllBranches().map(branch => {
					customBranchField.closest('.wrap').insertAdjacentHTML(
						'beforeend',
						utils.template.render(tpl.button.common, {
							class   : 'branch copy-branch',
							onclick : `utils.copyToClipboard('git checkout ${branch}')`,
							icon    : 'aui-iconfont-copy-clipboard',
							title   : branch,
						})
					);
				}),
				customBranchField.closest('.wrap').insertAdjacentHTML(
					'beforeend',
					utils.template.render(tpl.button.common, {
						id      : 'addBranchButton',
						class   : 'branch add-branch',
						onclick : 'APP.saveBranch()',
						// value   : '+',
						title   : 'Add branch',
						icon    : 'aui-iconfont-add',
					})
				)
			)
		)
	);


	//// Авто-мув
stp.DEBUG && console.log(`/secure/MoveIssue`, utils.location.has('/secure/MoveIssue') );
// https://jira.uid.me/secure/MoveIssue!default.jspa?id=51466
	utils.location.has('/secure/MoveIssue') && (
		document.jiraform.elements && (
			((WIN.PID = DOC.jiraform.elements.pid) && (WIN.P = DOC.jiraform.elements.project) && (WIN.PF = DOC.jiraform.elements['project-field'])
				&& (PID.value = P.value = 10200) && (PF.value = 'uCoz (UCOZ)'))
			|| ((WIN.IS = DOC.jiraform.elements.beanTargetStatusId) && (IS.value = 10106))
			|| ((!DOC.jiraform.elements.pid || !DOC.jiraform.elements.beanTargetStatusId) && (DOC.jiraform.elements.next_submit || DOC.jiraform.elements.move_submit))
		) && document.jiraform.submit()
	);


	//// кнопка фильтра текущей беты
stp.DEBUG && console.log(`WIN['create-menu']`, !!window['create-menu'], APP.getWeek(), APP.getCurrentBeta(DATE) );
	(CM = window['create-menu']) && (
		CM.insertAdjacentHTML('afterend', `<li><a class="aui-button aui-button-primary aui-style" href='/issues/?jql=${STP.filterMyOpened}'>myOpened</a>`),
		CM.insertAdjacentHTML('afterend',
			`<li><a class="aui-button aui-button-primary aui-style" href="/issues/?jql=${STP.filterCurrentBeta.replace(/\%s/g, APP.getCurrentBeta(DATE))}">currentBeta</a>`)
	);


	//// применение кастомных стилей
	utils.css.setStyle(APP.CSS.global);

})(
	window.APP = {
		// create branch name
		createBranch : (
			dev = (prompt(`Необходимо ввести номер своего dev!`, 23) || '23')
		) => `dev${dev.padStart(3, 0)}-${jira.app.issue.getIssueKey()}`,
		// write branch name
		writeBranch : (branchNameHTML,
			branchContainer = window.createBranchContainer,
			branchButton    = window.createBranchButton
		) => (branchButton && !branchButton.remove()) && !branchContainer.insertAdjacentHTML('beforeend', branchNameHTML),
		// save branch
		saveBranch : (
			branchForm  = new FormData(),
			branchName  = APP.createBranch(),
			allBranches = [ ...APP.getAllBranches(), branchName ],
			gitBranch   = utils.copyToClipboard(`git checkout -b ${branchName} origin/master`)
		) => {
			for (let item of Object.entries({
				customfield_10200    : allBranches.join(', '),
				issueId              : jira.app.issue.getIssueId(),
				atl_token            : atl_token(),
				singleFieldEdit      : true,
				fieldsToForcePresent : STP.BFID,
			}) ) branchForm.append(...item);

			self.fetch && fetch(`https://jira.uid.me/secure/AjaxIssueAction.jspa?decorator=none`, {method:'POST', credentials:'include', body:branchForm, } )
				.then(response => response.json() )
				.then(response => {
					response.errorCollection && !response.errorCollection.errorMessages.length
					&& (window.createBranchButton || window.addBranchButton).classList.toggle('success')
					&& setTimeout("location.reload();", 500)
				});
		},
		// get all branches name
		getAllBranches : field => {
			return (branchField = (field || window['customfield_10200-val'])) && branchField.textContent.trim().split(/[,|\s]+/g).filter(item => !!item) || [];
		},
		// get week of year
		getWeek : (
			now         = new Date(),
			today       = new Date(now.getFullYear(), now.getMonth(), now.getDate()),
			firstOfYear = new Date(now.getFullYear(), 0, 1),
		) => Math.ceil((((today - firstOfYear) / 86400000)-1)/7+1),
		// get beta number
		getCurrentBeta : (
			DATE = new Date(),
			WEEK = APP.getWeek(),
		) => +(DATE.getFullYear().toString().substr(2) + WEEK),

		// system CSS
		CSS : {
			global : `/* custom global CSS */
#jira #edit-issue-dialog.jira-dialog.jira-dialog-open .content{display:flex; flex-wrap:wrap; justify-content:space-between; }
#jira #edit-issue-dialog.jira-dialog.jira-dialog-open .content > :nth-child(19){order:-1; }

#createBranchButton.error:before,
#createBranchButton.success:before{padding:0; margin:0 7px 0 0; font:bold 1.1em/1 sans-serif; }
#createBranchButton.error:before{content:'\\2716'; color:brown; }
#createBranchButton.success:before{content:'\\2714'; color:green; }

#issue-content .issue-header-content .command-bar{padding:0 0 7px; }
#issue-content .issue-header-content .command-bar li.toolbar-item a{border-top-left-radius:0; border-top-right-radius:0; border-top:0; }

.aui-button.branch{padding:0 2px 1px 1px; margin:0 0 0 5px; height:22px; width:22px; text-align:center; }

@media screen and (max-width:1280px ) {
	#viewissuesidebar:not(:hover){width:50px; overflow:hidden; }
	#viewissuesidebar:not(:hover) .mod-header{transform:rotate(-90deg); margin:30px 0 0; }
	#viewissuesidebar:not(:hover) .mod-content{display:none; }
}
`,
		},
	},
	location, window, document, window.localStorage, window.sessionStorage,
	new Date(), new Date().getDate(), (new Date().getMonth() + 1), new Date().getFullYear(),
	window.STP = {
		// SETUP
		// DEBUG : 1,
		//// SETTINGS
		RFID : 'customfield_10009',
		BFID : 'customfield_10200',
		// filters
		filterCurrentBeta : `project = uCoz AND fixVersion = beta-%s ORDER BY cf[10009] DESC`,
		filterMyOpened    : `assignee = currentUser() AND status not in ("Ready for test", "In test", closed, done, "Ready for deploy") AND cf[10009] > 499 ORDER BY cf[10009] DESC`,
	},
	window.tpl = {
		button : {
			common : `<a id="{{id}}" href="javascript:;" class="aui-button {{class}}" ??onclick=onclick="{{onclick}}"?? title="{{title}}">
				<span class="??icon=aui-icon aui-icon-small {{icon}}??">{{value}}</span>
			</a>`,
		},
		fieldContainer : '<li class="item" id="{{listId}}" ><div class="wrap" id="{{wrapperId}}" ><strong class="name">{{name}}:</strong>{{value}} {{button}}</div></li>',
	},
);
